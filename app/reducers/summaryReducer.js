const dataEmpty = {
    pricing: {
		subtotal:0,
		savings: 0,
		tax:0,
		total:0,
		zip:0,
		discount:0
	},
	itemDetails:[
		{
			name:"",
			price:0,
			price_promo:0,
			quantity:0,
			id:0,
			picture:""
		}
	]
}
export default   (state = dataEmpty,action)=>{
    switch(action.type){
        case "SET_SUMARY":
			return {...state,...action.payload}
		case "SET_DISCOUNT":
			const pay = action.payload;
			let stat = state
			const discount = stat.pricing.subtotal * action.payload.discount
			stat.pricing.discount = discount
			stat.pricing.total = (stat.pricing.total - discount).toFixed(2)
			return {...stat,...action.payload}
        default:
            return state
    }
}