
const promoCode = [
	{
		code:123,
		discount:.10
	},
	{
		code:1234,
		discount:.10
	}
]
const pricingData = {
    pricing: {
		subtotal:102.96, // 99.11
		savings: 3.85,
		tax:8.92, // 8.92
		total:108.03, // 108.03
		zip:85050,
		discount:0
	},
	itemDetails:[
		{
			id:1,
			name:"Esssentials by OFM ESS-3085 Racing Style Leather Gaming Chair, Red",
			price:102.96,
			price_promo:99.11,
			quantity:1,
			picture:"https://res.cloudinary.com/walmart-labs/image/upload/w_960,dpr_auto,f_auto,q_auto:best/mg/gm/1p/images/product-images/img_large/00697001102719l.jpg"
		}
	]
}

export const getPurchaseSummary = (delay=1000)=>{
    return new Promise(function(resolve,reject){
        setTimeout(function(){
            resolve(pricingData)
        },delay)
    })
}

export const validateCodePromo = (code = 0)=>{
    return new Promise(function(resolve,reject){
		let result = promoCode.filter(el=>el.code == code)
		if(result.length > 0)
			resolve(result)
		else
			resolve(null)
    })
}
