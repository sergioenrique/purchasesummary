import React from 'react';
import {View,SafeAreaView,StatusBar,ScrollView} from 'react-native';
import { Provider } from "react-redux"
import { createStore } from "redux"
import Reducers from "./reducers"
import PurchaseSummary from "./components/PurchaseSummary"


const App = () => {
  return (
  <Provider store={createStore(Reducers)}>
      <View>
      <StatusBar/>
        <ScrollView>
          <PurchaseSummary />
        </ScrollView>
      </View>
  </Provider>
  )
};

export default App;
