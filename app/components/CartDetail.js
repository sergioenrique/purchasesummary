import React,{Component} from "react"
import {View,Text,StyleSheet,Image, ScrollView} from 'react-native';

class CartDetail extends Component {
    getListDetail(){
        const {ListDetail} = this.props
        let ListDetailData = ListDetail.map((item,key)=>{
            return (
                    <View style={{flex:1,flexDirection:"column"}} key={key}>
                        <View style={{flexDirection:"row",marginTop:10}}>
                            <View style={{flex:1,flexDirection:"column"}}>
                            <Image
                                style={{width: 100, height: 100}}
                                source={{uri: item.picture}}
                            />
                            </View>   
                            <View style={{flex:2,flexDirection:"column",justifyContent:"flex-start",alignItems:"flex-start"}}>
                                <Text style={{fontSize:15}} >{item.name}</Text>
                                <Text style={{fontSize:20,fontWeight:"bold"}} >${item.price_promo}</Text>
                                <Text style={{fontSize:20,fontWeight:"bold",textDecorationLine: "line-through"}} >${item.price}</Text>
                            </View>   
                        </View>
                    </View>
            )
        })
        return ListDetailData
    }
    render(){
        
        return (
            <View>
                {this.getListDetail()}
            </View>
        )
    }
}
export default CartDetail;