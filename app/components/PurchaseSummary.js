import React,{Component} from "react"
import {View,StyleSheet,Text,Button,TextInput,ScrollView} from 'react-native';
import {connect} from "react-redux"
import Pricing from "./Pricing"
import Loading from "./Loading"
import Divider from "./Divider"
import Expander from "./Expander"
import CartDetail from "./CartDetail"

import { getPurchaseSummary,validateCodePromo } from "../api"


class PurchaseSummary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            code: 0,
            apply:false
        };
      }

    componentDidMount(){
        this.getData()

    }

    validateCode = async(code)=>{
        const resp = await validateCodePromo(code);
        if(resp){
            this.setState({
                code,
                apply:true
            })
            this.props.dispatch({
                type:"SET_DISCOUNT",
                payload:resp[0]
            })
        }
    }

    getData = async()=>{
        let data = await getPurchaseSummary()
        this.props.dispatch({
            type:"SET_SUMARY",
            payload:data
        })
    }

    render(){
        return (
            <View style={styles.body}>
                {this.props.sumary.pricing.total == 0 ? 
                    <Loading message={"fetching data"}/>
                :
                    <View >
                        <Pricing data={this.props.sumary.pricing}/>
                        <Divider/>
                        <Expander 
                            titleCollapsed="See item details" 
                            titleExpanded="Hide item details">
                            <ScrollView>
                                <CartDetail ListDetail={this.props.sumary.itemDetails}/>
                            </ScrollView>
                        </Expander>
                        <Divider/>
                        <Expander 
                            titleCollapsed="Apply promo code" 
                            titleExpanded="Hide promo code">
                            <View style={{flex:1,flexDirection:"column"}}>
                                <View style={{flexDirection:"row",marginTop:10}}>
                                    <Text style={{fontSize:24,fontWeight:"bold"}}>Promo code</Text>
                                </View>
                                <View style={{flexDirection:"row",marginTop:10}}>
                                    <View style={{flex:2,flexDirection:"column"}}>
                                        
                                        <TextInput
                                        style={{ height: 35, borderColor: 'gray', borderWidth: 1 }}
                                        onChangeText={text => {
                                            this.setState({code:text,apply:false})
                                        }}/>
                                    </View>   
                                     <View style={{flex:1,flexDirection:"column",alignContent:"center",alignItems:"center"}}>
                                        <Button disabled={this.state.apply}  color="grey" title="Apply" onPress={() =>{this.validateCode(this.state.code)}}></Button>
                                    </View>   
                                </View>
                                        
                            </View>
                        </Expander>
                    </View>
                }
            </View>
        )
    }
}
/*
<Pricing data={this.props.sumary.pricing}/>*/

const styles = StyleSheet.create({
    body: {
      margin:"5%",
      marginTop:50
    },
});

const mapStateToProps = state =>{
    return {
        sumary:state.sumary
    }
}

export default connect(mapStateToProps)(PurchaseSummary)
