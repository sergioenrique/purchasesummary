import React,{Component} from "react"
import {View,Text,StyleSheet} from 'react-native';

class Divider extends Component {
    render(){
        return (
            <View style={{borderBottomColor: 'gray',borderBottomWidth: .5,marginTop:30,marginBottom:15}}/>
        )
    }
}
export default Divider;