import React,{Component} from "react"
import {StyleSheet,Text,View,Image,TouchableHighlight,Animated,ScrollView, TouchableOpacity} from 'react-native'

class Expander extends Component{
    constructor(props){
        super(props);
        this.icons = {
            'up'    : require('../assets/plus.png'),
            'down'  : require('../assets/minus.png')
        };

        this.state = { 
            title       : this.props.title,
            expanded    : true,
            height:0
        };
    }

    toggle(){
        this.setState({
            expanded : !this.state.expanded,
            height: this.state.height == 170 ? 0 :170,
        });
    }


    render(){
        
        let icon = this.icons['down'];

        if(this.state.expanded){
            icon = this.icons['up'];
        }

        return ( 
            <View style={styles.container} >
                <View style={styles.titleContainer}>
                    <Text onPress={this.toggle.bind(this)} style={styles.title} >{this.state.expanded ? this.props.titleCollapsed : this.props.titleExpanded}</Text>
                    <TouchableOpacity
                        style={styles.button} 
                        onPress={this.toggle.bind(this)}
                        underlayColor="#f1f1f1">
                        <Image
                            style={styles.buttonImage}
                            source={icon}
                        ></Image>
                    </TouchableOpacity>
                </View>
                
                <View style={styles.body} style={{height:this.state.height,marginTop:15}}>
                    <ScrollView>
                    {this.props.children}
                    </ScrollView>
                </View>

            </View>
        );
    }

}

var styles = StyleSheet.create({
    container   : {
        backgroundColor: '#fff',
        overflow:'hidden',
        marginTop:15,
        
    },
    titleContainer : {
        flexDirection: 'row'
    },
    title       : {
        color   :'#2a2f43',
        fontSize:20,
        textDecorationLine:'underline',
        paddingRight:20
    },
    button      : {

    },
    buttonImage : {
        width   : 30,
        height  : 25
    },
    body        : {
        padding     : 10,
        paddingTop  : 0
    }
});
export default Expander;