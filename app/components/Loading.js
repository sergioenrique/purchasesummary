import React,{Component} from "react"
import {View,Text,ActivityIndicator} from 'react-native';

class Loading extends Component {

    render(){
        return (
            <View style={{flex:1,height:"100%"}}>
                <View style={{flex:1,height:"100%",justifyContent:"center",alignItems:"center"}}>
                    <ActivityIndicator size="large" color="#0000ff" />
                    <Text>{this.props.message}</Text>
                </View>
            </View>
            
        )
    }
}

export default Loading