import React,{Component} from "react"
import {View,Text,StyleSheet,TouchableOpacity,Modal} from 'react-native';
import Divider from "./Divider"



class Pricing extends Component {
    constructor(props){
        super(props)
        this.state = {
            visible: false
        };
        
    }

    cambiarVisibilidad = ()=>{
        const val = !this.state.visible;
        this.setState({
            visible:val
        })
    }

    render(){
        return (
            <View style={{flex:1,flexDirection:"column"}}>
                <Modal transparent={true} visible={this.state.visible}>
                    <TouchableOpacity activeOpacity={1} style={{backgroundColor: 'rgba(52, 52, 52, 0.5)',padding: 0,flex: 1 }} onPress={this.cambiarVisibilidad}>
                        <View style={[
                        styles.triangleUp,
                        {
                            marginLeft: 15,
                            left: 0,
                            marginTop:110
                        }
                        ]} />
                        <View style={{borderRadius: 2,backgroundColor: 'white',flexDirection: 'row',marginLeft:15,marginRight:15}}>

                            <View style={{padding: 10, flex: 1}}>
                                <View style={{alignItems: 'center', justifyContent: 'center', margin: 10}}>
                                    <Text >{"Picking up your order in the store helps cut costs and we pass the savings on to you."}</Text>
                                </View>
                            </View>

                        </View>
                    </TouchableOpacity>
                </Modal>
                <View style={{flexDirection:"row",marginTop:10}}>
                    <View style={{flex:1,flexDirection:"column"}}>
                        <Text style={styles.subTitle}>Subtotal</Text>
                    </View>
                    <View style={{flex:1,flexDirection:"row-reverse"}}>
                        <Text style={styles.price}>${this.props.data.subtotal}</Text>
                    </View>
                </View>
                <View style={{flexDirection:"row",marginTop:10}}>
                    <Text on style={styles.subTitleUnderline} onPress={this.cambiarVisibilidad}>Pickup savings</Text>
                    <View style={{flex:1,flexDirection:"row-reverse"}}>
                        <Text style={styles.saving}>-${this.props.data.savings}</Text>
                    </View>
                </View>
                {
                this.props.data.discount != 0 ? (
                <View style={{flexDirection:"row",marginTop:10}}>
                    <View style={{flex:1,flexDirection:"column"}}>                    
                        <Text on style={styles.subTitleCoupon}>Discount coupon</Text>
                    </View>
                    <View style={{flex:1,flexDirection:"row-reverse"}}>
                        <Text style={styles.saving}>-${this.props.data.discount* 1}</Text>
                    </View>
                </View>
                ):null
                }
                <View style={{flexDirection:"row",marginTop:10}}>
                    <View style={{flex:1,flexDirection:"column"}}>
                        <Text style={styles.subTitle2}>Est. taxes & fees (Based on 97876)</Text>
                    </View>
                    <View style={{flex:1,flexDirection:"row-reverse"}}>
                        <Text style={styles.price}>${this.props.data.tax}</Text>
                    </View>
                </View>
                <Divider/>
                <View style={{flexDirection:"row"}}>
                    <View style={{flex:1,flexDirection:"column"}}>
                        <Text style={styles.subTitle3}>Est. total</Text>
                    </View>
                    <View style={{flex:1,flexDirection:"row-reverse"}}>
                        <Text style={styles.subTitle3}>${this.props.data.total}</Text>
                    </View>
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    subTitle: {
        fontSize: 24,
        fontWeight: '600',
      },
      subTitleCoupon: {
        fontSize: 24,
        fontWeight: '600',
        color:"grey"
      },
      subTitleUnderline: {
        fontSize: 24,
        fontWeight: '600',
        textDecorationLine:'underline'
      },
      subTitle2: {
        fontSize: 22,
        fontWeight: '600',
      },
      subTitle3: {
        marginTop:10,
        fontSize: 35,
        fontWeight: 'bold',
      },
      saving:{
        fontSize: 24,
        fontWeight: 'bold',
        color: "red",
        flexDirection:"row-reverse"
      },
      price:{
        fontSize: 24,
        fontWeight: 'bold',
        flexDirection:"row-reverse"
      },
      triangleUp: {
        width: 10,
        height: 10,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderTopWidth: 0,
        borderRightWidth: 10,
        borderBottomWidth: 10,
        borderLeftWidth: 10,
        borderBottomColor: 'white',
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderLeftColor: 'transparent',
      },
});

export default Pricing
